<?php
/**
 * @var $model page
 */
?>
<div id="main">
    <?php include __DIR__ . '/inc/content_item.ng-tpl.php'; ?>
    <?php include __DIR__ . '/inc/menu-item.ng-tpl.php'; ?>
    <div id="top_part">
        <div id="header">
            <div class="width_limit">
                <h1 id="title" ng-bind="model.meta.title"><?= $model->meta->title ?></h1>

                <div id="user_menu" data-ng-if="model.user">
                    <span ng-bind="model.user.login"><?= $model->user->login ?></span>

                    <div id="user_menu_drop">
                        <span ng-repeat="i in model.user_menu.items" data-href="{{i.url}}" ng-bind="i.name"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="width_limit">
            <div id="tabs">
                    <span ng-repeat="i in model.tabs.items" data-href="{{i.url}}" ng-class="{active:i.is_active}"
                          ng-bind="i.name"></span>
            </div>
            <div id="breadcrumbs" ng-show="model.breadcrumbs.items.length">
                <span ng-repeat="i in model.breadcrumbs.items" data-href="{{i.url}}" ng-bind="i.name"></span>
            </div>
            <div id="menu">
                <span class="menu" ng-repeat="i in model.menu.items" ng-include="'menu_item.html'"></span>
            </div>
            <div id="content">
                <div class="message" ng-repeat="m in model.messages.items" ng-class="{error:m.is_error}"
                     ng-bind="m.text"></div>
                <div class="content" ng-repeat="content in [model.content.items]"
                     ng-include="'content_item.html'"></div>

            </div>
        </div>
    </div>
    <div id="footer">
        <div class="width_limit">
            Время генерации страницы: {{ model.meta.time_output - model.meta.time_start }}
        </div>
    </div>
</div>