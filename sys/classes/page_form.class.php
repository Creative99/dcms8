<?php

/**
 * Class page_form_item
 */
class page_form_item
{
    public $type = null;
}

/**
 * Class page_form_control
 */
abstract class page_form_control extends page_form_item
{
    public $title = '';
    public $name = '';
}

/**
 * input
 * Class page_form_input
 */
class page_form_input extends page_form_control
{
    public $type = 'input';
    public $input_type = 'text';
    public $value = '';

    public $checked = false;
    public $disabled = false;

    /**
     * Установка значения, которое приходит из вне
     * TODO: запилить паттерны и сверять значение с ними
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}

/**
 * Textarea
 * Class page_form_textarea
 */
class page_form_textarea extends page_form_control
{
    public $type = 'textarea';
    public $value = '';

    /**
     * Установка значения, которое приходит из вне
     * TODO: запилить паттерны и сверять значение с ними
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}

/**
 * Элемент поля select
 * Class page_form_select_option
 */
class page_form_select_option extends page_form_control
{
    public
        $value = '',
        $text = '',
        $selected = false;
}

/**
 * Поле select
 * Class page_form_select
 * @property page_form_select_option[] options
 */
class page_form_select extends page_form_control
{
    public $type = 'select';
    public $options = array();

    function select($value)
    {
        foreach ($this->options AS $option) {
            $option->selected = $option->value == $value;
        }
    }

    public function getSelectedOption()
    {
        foreach ($this->options AS $option) {
            if ($option->selected)
                return $option;
        }
    }
}

/**
 * Форма
 * Class page_form
 * @property page_form_item[] items
 */
class page_form extends page_content_item
{
    public $type = 'form';
    public $action = '';
    public $method = 'post';
    public $items = array();

    /**
     * @param $title
     * @param string $name
     * @return page_form_input
     */
    public function addButton($title, $name = '')
    {
        $this->items[] = $button = new page_form_input();
        $button->input_type = 'submit';
        $button->value = $title;
        $button->name = $name;
        return $button;
    }

    /**
     * @param $title
     * @param $name
     * @param string $value
     * @return page_form_input
     */
    public function addTextField($title, $name, $value = '')
    {
        $this->items[] = $input = new page_form_input();
        $input->input_type = 'text';
        $input->name = $name;
        $input->title = $title;
        $input->value = $value;
        return $input;
    }

    /**
     * @param $title
     * @param $name
     * @param string $value
     * @return page_form_input
     */
    public function addPasswordField($title, $name, $value = '')
    {
        $this->items[] = $input = new page_form_input();
        $input->input_type = 'password';
        $input->name = $name;
        $input->title = $title;
        $input->value = $value;
        return $input;
    }

    /**
     * Применение занных с запроса от клиента
     */
    public function applyRequest()
    {
		// TODO: нужно придумать что то более изящное
        $args = $this->method === 'post' ? request::getPostVars() : request::getGetVars();

        foreach ($this->items AS $item) {
            if ($item instanceof page_form_select) {
                if (array_key_exists($item->name, $args))
                    $item->select($args[$item->name]);
                continue;
            }

            if ($item instanceof page_form_input || $item instanceof page_form_textarea) {
                if (array_key_exists($item->name, $args))
                    $item->setValue($args[$item->name]);
                continue;
            }

        }
    }

    /**
     * получение значения контрола после применения данных из запроса с клиента
     * @param string $input_name
     * @return string
     */
    public function getValue($input_name)
    {
        foreach ($this->items AS $item) {
            if ($item instanceof page_form_select) {
                if ($item->name == $input_name){
                    if ($option = $item->getSelectedOption()){
                        return $option->value;
                    }
                }
                continue;
            }

            if ($item instanceof page_form_input || $item instanceof page_form_textarea) {
                if ($item->name == $input_name)
                    return $item->value;
                continue;
            }
        }
    }

}