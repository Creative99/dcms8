<?php
class example_controller extends controller
{
    /**
     * @param $response \response
     */
    function get_index($response)
    {
        $page = $response->page;

        $page->setTitle("Заголовок");

        for ($i = 0; $i < 10; $i++) {
            $post = $page->content->addPost();
            $post->setUrl('post' . $i);
            $post->setIcon('cms');
            $post->setTitle(__("Добро пожаловать"));
            $post->setContentHtml('content');
            $post->setBottomHtml('bottom');
            $post->setTime(999999999);
            $post->counter = $i * 40;
            $post->highlight = !!$i % 2;

            for ($ii = 0; $ii < 10; $ii++) {
                $p_post = $post->addPost();
                $p_post->setTitle(__("Вложенный пост %s", $ii));
                //$p_post->setBottomHtml(rand(10000, 999999));
            }
        }
    }
}