<?php

class localization_controller extends controller
{

    /**
     * Определение текущей локали
     */
    function request_preprocess_locale_detect()
    {
        
    }

    /**
     * Модификация строки в соответствии с локалью
     * @param string $string
     */
    function hook_replace_string(&$string)
    {
        
    }

    /**
     * Модификация даты в соответствии с локалью
     * @param array $args
     */
    function hook_replace_date(&$args)
    {
        $date = &$args['date'];
        $format = $args['format'];
        $timestamp = $args['timestamp'];
    }
}