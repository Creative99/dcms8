<?php

/**
 * Хранение кэша контроллера
 * Изменения пишутся в файл перед завершением работы скрипта.
 * Сохранение параметров производится быстрее, чем в controller_settings
 * Рекомендуется использовать только для временных данных, которые не жалко потерять
 * Class controller_cache
 */
class controller_cache
{
    protected $_cache_name;

    /**
     * @param $module \module
     */
    function __construct($module)
    {
        $this->_cache_name = 'module-' . $module->getName();
    }

    /**
     * Получение значения из кэша
     * @param $name Имя параметра
     * @param mixed $default Это значение вернется, если отсутствует в кэше
     * @return mixed
     */
    function get($name, $default = false)
    {
        return cache_in_file::get($this->_cache_name, $name, $default);
    }

    /**
     * Запись значения в кэш
     * @param $name Имя параметра
     * @param $value Значение
     * @param $ttl Время жизни (в секундах)
     */
    function set($name, $value, $ttl = 0)
    {
        cache_in_file::set($this->_cache_name, $name, $value, $ttl);
    }

    /**
     * Очистка кэша
     */
    function clear()
    {
        cache_in_file::clear($this->_cache_name);
    }
}