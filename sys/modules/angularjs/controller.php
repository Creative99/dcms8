<?php

class angularjs_controller extends controller
{
    /**
     * @param $page_meta \page_meta
     * @return \page_meta
     */
    function theme_dependence_angular(page_meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel() . '/files/angular.js');
    }

    /**
     * @param $page_meta \page_meta
     */
    function theme_dependence_highcharts(page_meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel() . '/files/angular-highchart.js');
    }

    /**
     * @param $page_meta \page_meta
     */
    function theme_dependence_pagination(page_meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel() . '/files/angular-pagination.js');
    }

    /**
     * @param $page_meta \page_meta
     */
    function theme_dependence_elastic(page_meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel() . '/files/angular-elastic.js');
    }

}