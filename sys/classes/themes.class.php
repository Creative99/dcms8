<?php

/**
 * Список тем оформления
 */
abstract class themes
{

    static function getSettings()
    {
        static $settings = null;
        if ($settings === null) {
            $settings = array();
            $settings_json = @file_get_contents(SETTINGS_PATH . '/themes.json');
            if ($settings_json)
                $settings = json::decode($settings_json);
        }
        return $settings;
    }

    /**
     * Возвращает список установленных тем оформления
     * return \theme[]
     */
    public static function getInstalled()
    {
        $themes = array();
        $settings = themes::getSettings();
        foreach ($settings as $theme_name => $theme_config) {
            try {
                $themes[] = new theme(THEMES_PATH . '/' . $theme_name);
            } catch (Exception $e) {
                
            }
        }
        return $themes;
    }

    /**
     * Возвращает одну из поддерживаемых тем оформления среди установленных
     * @return \theme
     */
    public static function getSupportedTheme()
    {
        $themes = themes::getInstalled();
        return $themes[0];
    }
}