<?php

version_compare(PHP_VERSION, '5.3', '>=') or die('Требуется PHP >= 5.3');

if (@function_exists('ini_set')) {
	// игнорировать повторяющиеся ошибки
	ini_set('ignore_repeated_errors', true);

	// показываем только фатальные ошибки
	ini_set('error_reporting', E_ERROR);

	// непосредственно, включаем показ ошибок
	ini_set('display_errors', true);
}

/**
 * @const H путь к корневой директории сайта
 */
define('H', __DIR__);

require_once H . '/sys/inc/start.php';

session_start();

try {
	modules::executePreprocess();
	$response = modules::executeRequest();
	modules::executePostprocess($response);

	$response->output();
} catch (ResponseException $e) {
	$response = new response();
	$response->page->setTitle($e->getTitle());
	$response->page->setContent($e->getContent());

	//$response->page->err($e->getMessage());
	$response->output();
} catch (Exception $e) {
	//$response = new response();
	//$response->page->setTitle(__("Ошибка"));
	//$response->page->err($e->getMessage());
	//$response->page->err("Произошла неведомая херня");
	//$response->output();

	header("Content-Type: text/plain");
	print_r($e);
}

if (@function_exists('ini_set')) {
	// показываем только фатальные ошибки
	ini_set('error_reporting', 0);

	// непосредственно, включаем показ ошибок
	ini_set('display_errors', false);
}