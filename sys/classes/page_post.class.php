<?php

/**
 * Class page_post_time
 * @property int $timestamp
 * @property bool $show
 * @property string $format
 */
class page_post_time extends page_render {

    public
            $show = false,
            $timestamp,
            $format = 'd MMM yy H:mm:ss';

}

/**
 * Class page_post_action
 * @property string $name
 * @property string $url
 */
class page_post_action extends page_render {

    public
            $name,
            $url;

}

/**
 * Class page_post_actions
 * @property page_post_action items
 */
class page_post_actions extends page_render {

    public $items = array();

}

/**
 * Class page_post
 * @property boolean highlight
 * @property string image_src
 * @property string icon_class
 * @property string title
 * @property string url
 * @property \page_post_actions actions
 * @property string content_html
 * @property string bottom_html
 * @property \page_post_time time
 * @property string counter
 */
class page_post extends page_content_item {

    public $type = 'post';
    public
            $highlight = false,
            $image_html = "",
            $icon_html = "",
            $title = "",
            $url = "",
            $actions,
            $content_html = "",
            $bottom_html = "",
            $time,
            $counter = "";
    protected 
            $_image_src = "",
            $_icon_src = "";

    function __construct() {
        parent::__construct();
        $this->actions = new page_post_actions();
        $this->time = new page_post_time();
    }

    public function getIconSrc(){
        return $this->_icon_src;
    }
    
    /**
     * @param string $rel_path
     */
    public function setIconSrc($rel_path, $replace_image = true) {
        $this->_icon_src = $rel_path;
        if ($this->_icon_src) {
            $this->icon_html = '<img src="' . htmlspecialchars($rel_path) . '" alt="' . htmlspecialchars(basename($rel_path)) . '" />';

            if ($replace_image) {
                $this->image_html = "";
                $this->_image_src = "";
            }
        } else {
            $this->icon_html = "";
        }
        modules::executeHook('page_post.setIconSrc', $this);
    }

    public function getImageSrc(){
        return $this->_image_src;
    }
    
    /**
     * @param string $rel_path
     */
    public function setImageSrc($rel_path, $replace_icon = true) {
        $this->_image_src = $rel_path;
        if ($this->_image_src) {
            $this->image_html = '<img src="' . htmlspecialchars($rel_path) . '" alt="' . htmlspecialchars(basename($rel_path)) . '" />';
            if ($replace_icon) {
                $this->icon_html = "";
                $this->_icon_src = "";
            }
        } else {
            $this->image_html = "";
        }
        modules::executeHook('page_post.setImageSrc', $this);
    }

    /**
     * @param int|\page_post_time $time
     */
    public function setTime($time) {
        if ($time instanceof page_post_time) {
            $this->time = $time;
        } else {
            $this->time->timestamp = (int) $time;
            $this->time->show = true;
        }
    }

    /**
     * @param string $str
     */
    public function setTitle($str) {
        $this->title = $str;
    }

    /**
     * @param string $content_html
     */
    public function setContentHtml($content_html) {
        $this->content_html = $content_html;
    }

    /**
     * @param string $bottom_html
     */
    public function setBottomHtml($bottom_html) {
        $this->bottom_html = $bottom_html;
    }

    /**
     * @return page_post
     */
    public function addPost() {
        return $this->content_items[] = new page_post();
    }

    /**
     * @param string $string
     */
    public function setUrl($string) {
        $this->url = $string;
    }
    

}
