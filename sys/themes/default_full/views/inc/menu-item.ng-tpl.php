<script type="text/ng-template" id="menu_item.html">
    <span class="menu_item" data-href="{{data.url}}" ng-bind="data.name"></span>
    <span class="menu" ng-if="data.menu_items.length > 0" ng-repeat="data in data.menu_items"
          ng-include="'menu_item.html'"></span>
</script>