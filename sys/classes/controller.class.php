<?php

/**
 * Class controller
 * @property \module $module
 */
class controller
{
    public
            $module;

    /**
     * @param $module \module
     */
    function __construct($module)
    {
        $this->module = $module;
    }

    /**
     * Возвращает относительный путь к папке модуля.
     * Можно использовать для вывода в URL
     * @return string
     */
    function getCurrentPathRel()
    {
        return '/sys/modules/' . $this->module->getName();
    }

    /**
     * Возвращает абсолютный путь к папке модуля.
     * Используется в серверной части
     * @return string
     */
    function getCurrentPathAbs()
    {
        return H . '/sys/modules/' . $this->module->getName();
    }

    /**
     * Получение объекта для манипуляции с кэшем модуля
     * @return controller_cache
     */
    function getCache()
    {
        return $this->module->getControllerCache();
    }

    /**
     * Получение объекта для манипуляции с настройками модуля
     * @return controller_settings
     */
    function getSettings()
    {
        return $this->module->getControllerSettings();
    }

    function __destruct()
    {
        unset($this->module);
    }
}
