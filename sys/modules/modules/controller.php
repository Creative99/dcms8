<?php
class modules_controller extends controller
{
    /**
     * @param $response \response
     */
    function get_modules($response)
    {

        switch ($this->request->path_arr[3]) {
            case 'installed':
                $tab_sel = 'installed';
                $modules = modules::getInstalled();
                break;
            default:
                $tab_sel = 'all';
                $modules = modules::getAll();
                break;
        }

        $response->page->setTitle('Список модулей');
        $response->page->breadcrumbs->add(__('Админка'), '/admin/');

        $response->page->tabs->addTab(__('Все'), '/admin/modules/all/', $tab_sel == 'all');
        $response->page->tabs->addTab(__('Установленные'), 'admin/modules/installed/', $tab_sel == 'installed');

        foreach ($modules AS $module) {
            $post = $response->page->content->addPost();
            $post->title = $module->getName();
            $post->setUrl('/admin/modules/module/' . $tab_sel . '/' . $module->getName());
        }

    }


    /**
     * @param $response \response
     */
    function get_module($response)
    {
        $module = modules::getByName($this->request->path_arr[5]);
        $response->page->breadcrumbs->add(__('Админка'), '/admin/');
        $response->page->breadcrumbs->add(__('Модули'), '/admin/modules/' . $this->request->path_arr[4] . '/');

        if (!$module) {
            $response->page->setTitle(__('Модуль не найден'));
            $response->page->err(__('Модуль не найден'));
            return;
        }

        $response->page->setTitle(__('Модуль "%s"', $module->getName()));

    }

    /**
     * @param $response \response
     */
    function get_priority($response)
    {
        switch ($this->request->path_arr[4]) {
            case 'request':
                $tab_sel = 'request';
                $modules = modules::getRequestPreprocessModules();
                break;
            case 'response':
                $tab_sel = 'response';
                $modules = modules::getResponsePostprocessModules();
                break;
            default:
                $tab_sel = 'controller';
                $modules = modules::getInstalled();
                break;
        }

        $response->page->setTitle('Приоритет модулей');
        $response->page->breadcrumbs->add(__('Админка'), '/admin/');

        $response->page->tabs->addTab(__('Контроллер'), '/admin/modules/priority/controller/', $tab_sel == 'controller');
        $response->page->tabs->addTab(__('Обработка ответов'), '/admin/modules/priority/response/', $tab_sel == 'response');
        $response->page->tabs->addTab(__('Обработка запросов'), '/admin/modules/priority/request/', $tab_sel == 'request');


        foreach ($modules AS $module) {
            $routes = $module->getRoutes();
            if (!count($routes))
                continue;

            $post = $response->page->content->addPost();
            $post->title = $module->getName();

            switch ($tab_sel) {
                case 'controller':
                    foreach ($routes as $route) {
                        $post_child = $post->addPost();
                        $post_child->setTitle($route['url']);
                        $post_child->setContentHtml($route['method']);
                    }
                    break;
                case 'request':
//                    $post_child = $post->addPost();
//                    $post_child->setTitle($module->getRequestPreprocessUrl());
//                    $post_child->setContentHtml($module->getRequestPreprocessMethodName());
                    break;
                case 'response':
//                    $post_child = $post->addPost();
//                    $post_child->setTitle($module->getResponsePostprocessUrl());
//                    $post_child->setContentHtml($module->getResponsePostprocessMethodName());
                    break;
            }


        }

    }

    /**
     * Обрабатывается только для /admin/
     * @param \response $response
     */
    function response_postprocess_admin($response)
    {
        $content = $response->page->content;

        $post = $content->addPost();
        //$post->setUrl('admin/modules/');
        $post->setTitle(__('Модули'));

        $child_post = $post->addPost();
        $child_post->setTitle(__('Список модулей'));
        $child_post->setUrl('/admin/modules/all/');

        $child_post = $post->addPost();
        $child_post->setTitle(__('Приоритет модулей'));
        $child_post->setUrl('/admin/modules/priority/controller/');
    }
}