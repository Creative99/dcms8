<?php

class page_render_view
{
    static protected $_instance = null;

    protected $_theme;
    protected $_views_path = ''; // папка с файлами шаблонов
    protected $_assigned = array(); // переменные, которые будут переданы в шаблон


    static public function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new self();
        return self::$_instance;
    }

    protected function __construct()
    {
        $this->_theme = themes::getSupportedTheme();
    }

    /**
     * Установка переменной в шаблон
     * @param string $name Ключ переменной
     * @param mixed $value Значение
     */
    protected function _assign($name, $value = null)
    {
        if (is_array($name)) {
            foreach ($name as $key => $value) {
                $this->_assign($key, $value);
            }
            return;
        }
        if (is_scalar($name))
            $this->_assigned[$name] = $value;
    }

    /**
     * Получение обработанного шаблона
     * @param string $tpl_file Путь к файлу шаблона или его имя, если указан dir_template
     * @return string HTML код после обработки
     */
    protected function _fetch($tpl_file)
    {
        if (($tpl_path = $this->_getTemplatePath($tpl_file)) === false) {
            return null;
        }
        extract($this->_assigned);
        ob_start();
        @include $tpl_path;
        $content = ob_get_clean();
        return $content;
    }

    /**
     * получение пути к файлу шаблона
     * @param string $tpl_name Путь к файлу шаблона или его имя, если указан dir_template
     * @return string Путь к файлу шаблона
     */
    protected function _getTemplatePath($tpl_name)
    {
        return $this->_theme->getViewsPathAbs() . '/' . basename($tpl_name, '.php') . '.php';
    }

    /**
     * @param page_render $model
     * @param string $view
     * @return string
     */
    public function render($model, $view)
    {
        $this->_assign('model', $model);
        return $this->_fetch($view);
    }

    /**
     * @return \theme
     */
    public function getTheme()
    {
        return $this->_theme;
    }
}

/**
 * Реализация отрисовки модели указанной вьюхой
 * Class page_render
 */
class page_render
{
    public $id;

    function __construct()
    {
        $this->id = $this->_getNewId();
    }

    /**
     * Возвращает уникальный идентификатор класса на странице
     * @staticvar array $id
     * @return string
     */
    protected function _getNewId()
    {
        static $id = array();
        $class = get_class($this);
        return $class . '_' . @++$id[$class];
    }


    /**
     * Отрисовка указанной вьюхи по текущей модели
     * @param string $view
     * @return string HTML
     */
    public function render($view)
    {
        $render = page_render_view::getInstance();
        return $render->render($this, $view);
    }
} 