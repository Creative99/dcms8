<?php

class system_scripts_controller extends controller
{
    function theme_dependence_dcms_client_model(page_meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel() . '/files/dcms_client_model.js');
    }
}