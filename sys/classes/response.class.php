<?php

/**
 * Class ResponseException
 * @property \page_content $_content
 * @property string $_title
 */
class ResponseException extends Exception
{
    protected $_title, $_content;

    function __construct()
    {
        $this->_content = new page_content();
    }

    /**
     * @return string
     */
    function getTitle()
    {
        return $this->_title;
    }

    /**
     * @return page_content
     */
    function getContent()
    {
        return $this->_content;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    }
}

class Response404Exception extends ResponseException
{
    
}

class Response403Exception extends ResponseException
{
    
}

/**
 * Ответ сервера
 * В зависимости от запроса, возвращает отрендеренный html код или json модели страницы.
 * Class response
 * @property \page $page
 */
class response
{
    public
            $page,
            $last_modified;

    function __construct()
    {
        $this->page = new page();
        $this->last_modified = time();
    }

    /**
     */
    function output()
    {
        header('Cache-Control: no-store, no-cache, must-revalidate', true);
        header('Expires: ' . date('r'), true);
        header("Last-Modified: " . gmdate("D, d M Y H:i:s", $this->last_modified) . " GMT", true);

        $this->page->meta->time_output = microtime(true);

        if ($content = ob_get_clean())
            $this->page->content->items[] = new page_content_html($content);

        if (AJAX)
            $this->_output_json();
        else
            $this->_output_html();
    }

    /**
     */
    protected function _output_json()
    {
        header('Content-Type: application/json; charset=utf-8', true);

        echo $this->page->getJson();
    }

    protected function _output_html()
    {
        header('X-UA-Compatible: IE=edge', true);
        header('Content-Type: text/html; charset=utf-8', true);

        echo $this->page->render('page.view');
    }
}