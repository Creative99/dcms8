<?php

class highcharts_controller extends controller
{
    /**
     * @param $page_meta \page_meta
     * @return \page_meta
     */
    function theme_dependence_highcharts($page_meta)
    {
        $page_meta->scripts[] = $this->getCurrentPathRel() . '/files/highcharts.js';
    }

}