<?php
/** @var $model page */
?>
<title ng-bind="model.meta.title"><?= $model->meta->title ?></title>
<script>
    var page_data = <?=$model->getJson();?>;
    var translates = {};
    var codes = {};
</script>
<link rel="shortcut icon" href="<?= $model->meta->favicon ?>"/>
<?php
foreach ($model->meta->styles AS $style_href) {
    echo "<link rel='stylesheet' href='$style_href' type='text/css'/>" . PHP_EOL;
}
foreach ($model->meta->scripts AS $script_src) {
    echo "<script charset='utf-8' src='$script_src' type='text/javascript'></script>" . PHP_EOL;
}
?>
<meta http-equiv="Сontent-Type" content="application/xhtml+xml; charset=utf-8"/>
<meta name="generator" content="DCMS <?= system::getProperty('version') ?>"/>
<style type="text/css">
    .ng-hide {
        display: none !important;
    }
</style>