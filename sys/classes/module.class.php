<?php

class module
{
    protected static
            $prefix_get = 'get_',
            $prefix_post = 'post_',
            $prefix_hook = 'hook_',
            $prefix_request_preprocess = 'request_preprocess_',
            $prefix_response_postprocess = 'response_postprocess_',
            $prefix_theme_dependence = 'theme_dependence_';
    protected
            $_config,
            $_abs_path,
            $_name = "",
            $_controller_name,
            $_controller_instance,
            $_controller_filename,
            $_controller_settings,
            $_controller_cache,
            $_routes = array(),
            $_dependencies = array(),
            $_theme_dependencies = array(),
            $_hooks = array(),
            $_request_preprocess = array(),
            $_response_postprocess = array(),
            $_db_structure = array(),
            $_classes = array();

    /**
     * @param $abs_path string
     */
    function __construct($abs_path, $installed_config = null)
    {
        $this->_abs_path = $abs_path;
        $this->_loadConfig($installed_config);
    }

    /**
     * @return array
     */
    protected function _getRequestMethodHandler()
    {
        $routes = $this->getRoutes();
        foreach ($routes AS $route_url_pattern => $method) {
            if (preg_match('#^' . $route_url_pattern . '$#', request::getPath()))
                return $method;
        }

        return null;
    }

    /**
     * @return boolean
     */
    public function canExecuteRequest()
    {
        return !!$this->_getRequestMethodHandler();
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->_routes;
    }

    /**
     * @throws Exception
     */
    private function _loadConfig($installed_config = null)
    {
        if (false === ($path = realpath($this->_abs_path)))
            throw new Exception(__("Не удалось открыть модуль по указанному пути"));

        if (false === ($conf_path = realpath($path . '/config.json')))
            throw new Exception(__("Не удалось найти конфиг модуля по указанному пути"));

        $this->_name = basename($path);

        if ($installed_config) {
            $config = $installed_config;
        } else {
            if (false === ($config_json = @file_get_contents($conf_path)))
                throw new Exception(__("Не удалось прочитать конфиг модуля по указанному пути"));

            try {
                $config = json::decode($config_json);
            } catch (Exception $e) {
                throw new Exception(__("При чтении конфига произошла ошибка: %s", $e->getMessage()));
            }
        }

        $this->_controller_filename = basename($config['controller_filename']);
        $this->_controller_name = $this->_name . '_controller';
        $this->_config = $config;
        $this->_parseConfig();
    }

    /**
     * Парсинг конфига. При любом отклонении от формата выкидываем информативное исключение.
     * @throws Exception
     */
    protected function _parseConfig()
    {
        /*
         * обработчики запросов
         */
        if (array_key_exists('routes', $this->_config)) {
            if (!is_array($this->_config['routes']))
                throw new Exception(__('Свойство %s должно быть ассоциативным массивом (в json - объект вида { "[%s]" : "[%s]"})', 'routes', 'url', 'метод контроллера'));
            $this->_routes = $this->_config['routes'];
        }

        /*
         * обработчики хуков
         */
        if (array_key_exists('hooks', $this->_config)) {
            if (!is_array($this->_config['hooks']))
                throw new Exception(__('Свойство %s должно быть ассоциативным массивом (в json - объект вида { "[%s]" : "[%s]"})', 'hooks', 'hook name', 'метод контроллера'));
            $this->_hooks = $this->_config['hooks'];
        }

        /*
         * зависимости от других модулей
         */
        if (array_key_exists('dependencies', $this->_config)) {
            if (!is_array($this->_config['dependencies']))
                throw new Exception(__('Свойство %s должно быть ассоциативным массивом (в json - объект вида { "[%s]" : "[%s]"})', 'dependencies', 'имя модуля', 'минимальная версия'));
            $this->_dependencies = $this->_config['dependencies'];
        }

        /*
         * зависимости тем оформлений, которые могут быть удовлетворены.
         * предполагается использовать для подключения различных js библиотек, которые могут централизованно обновляться
         */
        if (array_key_exists('theme_dependencies', $this->_config)) {
            if (!is_array($this->_config['theme_dependencies']))
                throw new Exception(__('Свойство %s должно быть ассоциативным массивом (в json - объект вида { "[%s]" : "[%s]"})', 'theme_dependencies', 'зависимость, которая может быть удовлетворена в теме оформления', 'метод контроллера'));
            $this->_theme_dependencies = $this->_config['theme_dependencies'];
        }

        /*
         * обработчики всех запросов
         */
        if (array_key_exists('request_preprocess', $this->_config)) {
            if (!is_array($this->_config['request_preprocess']))
                throw new Exception(__('Свойство %s должно быть ассоциативным массивом (в json - объект вида { "[%s]" : "[%s]"})', 'request_preprocess', 'url', 'метод контроллера'));
            $this->_request_preprocess = $this->_config['request_preprocess'];
        }

        /*
         * обработчики ответов
         */
        if (array_key_exists('response_postprocess', $this->_config)) {
            if (!is_array($this->_config['response_postprocess']))
                throw new Exception(__('Свойство %s должно быть ассоциативным массивом (в json - объект вида { "[%s]" : "[%s]"})', 'response_postprocess', 'url', 'метод контроллера'));
            $this->_response_postprocess = $this->_config['response_postprocess'];
        }

        /*
         * используемая структура в базе данных
         */
        if (array_key_exists('db_structure', $this->_config)) {
            if (!is_array($this->_config['db_structure']))
                throw new Exception(__('Свойство %s должно быть ассоциативным массивом (в json - объект вида { "[%s]" : "[%s]"})', 'db_structure', 'название таблицы', 'параметры'));
            $this->_db_structure = $this->_config['db_structure'];
        }
    }

    /**
     * @return controller
     * @throws Exception
     */
    public function getControllerInstance()
    {
        if (!$this->_controller_instance && $this->_controller_filename) {
            include_once $this->_abs_path . '/' . $this->_controller_filename;
            $this->_controller_instance = new $this->_controller_name($this);
        }

        if (!$this->_controller_instance)
            throw new Exception(__("Контроллер для модуля не задан"));

        if (!($this->_controller_instance instanceof controller))
            throw new Exception(__("Контроллер должен быть наследником класса controller"));

        return $this->_controller_instance;
    }

    /**
     * Предварительная обработка запроса
     */
    public function executePreprocess()
    {
        $method = $this->getRequestPreprocessMethodName();
        $this->getControllerInstance()->$method();
    }

    /**
     * Выполнение запроса
     * @param $response \response
     */
    public function executeRequest(response $response)
    {
        // список имен методов, которые будут проверяться у контроллера
        $execute_methods = array();
        $method_handler = $this->_getRequestMethodHandler();

        switch (request::getMethod()) {
            case 'get':
                $execute_methods[] = self::$prefix_get . $method_handler;
                break;
            case 'post':
                $execute_methods[] = self::$prefix_post . $method_handler;
                $execute_methods[] = self::$prefix_get . $method_handler;
                break;
        }


        $instance = $this->getControllerInstance();
        
        $rc = new ReflectionClass($this->_controller_name);

        $methods = $rc->getMethods(ReflectionMethod::IS_PUBLIC);
        foreach ($methods AS $rMethod) {
            foreach ($execute_methods AS $checking_method) {
                if ($rMethod->getName() === $checking_method)
                    call_user_func(array($instance, $rMethod->getName()), $response);
            }
        }
    }

    /**
     * Выполнение постобработчика ответа
     * @param response $response
     */
    public function executePostprocess(response $response)
    {
        $method = $this->getResponsePostprocessMethodName();
        $this->getControllerInstance()->$method($response);
    }

    /**
     * Выполнение хука.
     * Данные передаются по ссылке
     * @param sting $hook_name
     * @param mixed $hook_data
     */
    public function executeHook($hook_name, &$hook_data = array())
    {
        $method = $this->_getHookMethodName($hook_name);
        $this->getControllerInstance()->$method($hook_data);
    }

    /**
     * @return string|null
     */
    public function getRequestPreprocessMethodName()
    {
        foreach ($this->_request_preprocess AS $url_pattern => $method_name) {
            if ($url_pattern && $method_name && preg_match('#^' . $url_pattern . '$#', request::getPath()))
                return self::$prefix_request_preprocess . $method_name;
        }
        return null;
    }

    /**
     * @return string|null
     */
    public function getResponsePostprocessMethodName()
    {
        foreach ($this->_response_postprocess AS $url_pattern => $method_name) {
            if ($url_pattern && $method_name && preg_match('#^' . $url_pattern . '$#', request::getPath()))
                return self::$prefix_response_postprocess . $method_name;
        }
        return null;
    }

    /**
     * Возвращает имя класса - контроллера
     * @return string
     */
    public function getControllerName()
    {
        return $this->_controller_name;
    }

    /**
     * Возвращает имя модуля
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Возвращает объект для манипуляции с настройками модуля
     * @return \controller_settings
     */
    public function getControllerSettings()
    {
        if (!$this->_controller_settings)
            $this->_controller_settings = new controller_settings($this);
        return $this->_controller_settings;
    }

    /**
     * Возвращает объект для манипуляции с кэшем модуля
     * @return \controller_cache
     */
    public function getControllerCache()
    {
        if (!$this->_controller_cache)
            $this->_controller_cache = new controller_cache($this);
        return $this->_controller_cache;
    }

    /**
     * Список зависимостей от других модулей.
     * Должен ассоциативный массив вида [название модуля => минимальная версия, ...], но пока возвращает просто массив
     * @return string[]
     */
    public function getDependencies()
    {
        return $this->_dependencies;
    }

    /**
     * Возвращает метод-обработчик зависимости для темы оформления
     * @param string $dependence_name
     * @return string
     */
    public function getThemeDependenceHandler($dependence_name)
    {
        if (!array_key_exists($dependence_name, $this->_theme_dependencies))
            return false;
        return self::$prefix_theme_dependence . $this->_theme_dependencies[$dependence_name];
    }

    /**
     * Возвращает список хуков
     * @return string[]
     */
    public function getHooks()
    {
        return array_keys($this->_hooks);
    }

    /**
     * Возвращает массив классов, которые могут быть использованы бругими модулями.
     * @return string[string]
     */
    public function getClasses()
    {
        $classes = array();
        foreach ($this->_classes as $class_name => $file_name) {
            $classes[$class_name] = $this->_abs_path . '/' . $file_name;
        }
        return $classes;
    }

    /**
     * @param $hook_name
     * @return string|false
     */
    protected function _getHookMethodName($hook_name)
    {
        if (!array_key_exists($hook_name, $this->_hooks))
            return false;
        return $this->_hooks[$hook_name];
    }

    /**
     * Возвращает список возможных зависиместей для тем оформления, которые могут быть обработаны данным модулем.
     * @return string[]
     */
    public function getThemeDependencies()
    {
        return array_keys($this->_theme_dependencies);
    }

    /**
     * Возвращает массив структур таблиц для базы данных, необходимых для работы данного модуля.
     * @return \db_table[]
     */
    public function getDbStructure()
    {
        $db_tables = array();
        foreach ($this->_db_structure AS $table_config) {
            $db_tables[] = new db_table($table_config);
        }

        return $db_tables;
    }

    /**
     * Возвращает конфиг модуля в виде ассоциативного массива
     * @return array
     */
    public function getConfig()
    {
        return $this->_config;
    }
}