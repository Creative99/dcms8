<?php

class breadcrumbs_controller extends controller
{
    /**
     * @param $response \response
     * @return \response
     */
    function response_postprocess_add_breadcrumbs($response)
    {
        if (request::getPath() != '/') {
            $home_breadcrumb = new page_breadcrumb(__('Главная'), '/');
            $home_breadcrumb->is_home = true;
            array_splice($response->page->breadcrumbs->items, 0, 0, array($home_breadcrumb));
        }

        //$current_breadcrumb = new page_breadcrumb($response->page->meta->title, request::getPath());
        //$current_breadcrumb->is_current = true;
        //$response->page->breadcrumbs->items[] = $current_breadcrumb;

        return $response;
    }
}