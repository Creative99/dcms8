<?php

/**
 * @const TIME_START Время запуска скрипта в миллисекундах
 */
define('TIME_START', microtime(true)); // время запуска скрипта

/**
 * @const AJAX скрипт вызван AJAX запросом
 */
define('AJAX', strtolower(@$_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

/**
 * @const IS_WINDOWS Запущено ли на винде
 */
if (!defined('IS_WINDOWS')) {
	define('IS_WINDOWS', strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
}

// устанавливаем Московскую временную зону по умолчанию
if (@function_exists('ini_set')) {
	ini_set('date.timezone', 'Europe/Moscow');
}

/**
 * @const TMP временная папка
 */
define('TMP', H . '/sys/tmp');
/**
 * @const TIME UNIXTIMESTAMP
 */
define('TIME', time());
/**
 * @const MODULES_PATH Путь к папке с модулями
 */
define('MODULES_PATH', H . '/sys/modules');
/**
 * @const THEMES_PATH Путь к папке с темами оформления
 */
define('THEMES_PATH', H . '/sys/themes');
/**
 * @const SETTINGS_PATH Путь к папке с настройками
 */
define('SETTINGS_PATH_DEFAULT', H . '/sys/settings/default');
/**
 * @const SETTINGS_PATH Путь к папке с настройками
 */
define('SETTINGS_PATH', H . '/sys/settings/installed');
/**
 * @const CLASSES_PATH Путь к папке с системными классами
 */
define('CLASSES_PATH', H . '/sys/classes');

require_once(CLASSES_PATH . '/cache.class.php');
require_once(CLASSES_PATH . '/module.class.php');
require_once(CLASSES_PATH . '/modules.class.php');
require_once(CLASSES_PATH . '/classes.class.php');

/**
 * автоматическая загрузка классов
 */
spl_autoload_register(array('classes', 'autoLoadClass'));

/**
 * @param *
 * @return string
 */
function __()
{
	$args = func_get_args();
	$args_num = count($args);
	if (!$args_num) {
		// нет ни строки ни параметров, вообще нихрена
		return '';
	}

	modules::executeHook('__', $args[0]);

	if ($args_num == 1) {
		// строка без параметров
		return $args[0];
	}

	$args4eval = array();
	for ($i = 1; $i < $args_num; $i++) {
		$args4eval[] = '$args[' . $i . ']';
	}
	return eval('return sprintf($args[0],' . implode(',', $args4eval) . ');');
}