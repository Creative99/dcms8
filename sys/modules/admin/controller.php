<?php
class admin_controller extends controller
{
    /**
     * Обработчик запроса /admin/
     * Ссылки на системные настройки добавляются в модулях в методах response_postprocess
     * @param $response \response
     * @return response
     */
    function get_index($response)
    {
        $response->page->setTitle(__('Админка'));
    }
}