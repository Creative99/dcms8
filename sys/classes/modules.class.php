<?php

/**
 * Сортировка модулей
 * Class modules_cmp
 */
abstract class modules_cmp
{
    const SORT_BY_CONTROLLER = 0;
    const SORT_BY_REQUEST = 1;
    const SORT_BY_RESPONSE = 2;
    const SORT_BY_HOOK = 2;

    static protected function _cmp_by_field($v1, $v2, $field)
    {
        return ($v1[$field] == $v2[$field]) ? 0 : ($v1[$field] > $v2[$field] ? 1 : -1);
    }

    static protected function _cmp_controller($v1, $v2)
    {
        return self::_cmp_by_field($v1, $v2, 'controller');
    }

    static protected function _cmp_request($v1, $v2)
    {
        return self::_cmp_by_field($v1, $v2, 'request');
    }

    static protected function _cmp_response($v1, $v2)
    {
        return self::_cmp_by_field($v1, $v2, 'response');
    }

    static protected function _cmp_hook($v1, $v2)
    {
        return self::_cmp_by_field($v1, $v2, 'hook');
    }
}

/**
 * Управление модулями
 * Class modules
 */
abstract class modules extends modules_cmp
{
    static protected $_settings = null;

    static protected function _getSettings()
    {
        if (self::$_settings === null) {
            self::$_settings = array();

            if (file_exists(SETTINGS_PATH . '/modules.json')) {
                $settings_json = @file_get_contents(SETTINGS_PATH . '/modules.json');
                if ($settings_json)
                    self::$_settings = json::decode($settings_json);
            } else {
                $settings_json = @file_get_contents(SETTINGS_PATH_DEFAULT . '/modules.json');
                if ($settings_json)
                    self::$_settings = json::decode($settings_json);
            }
        }
        return self::$_settings;
    }

    static protected function _setSettings($settings = null)
    {
        if (is_null($settings))
            throw new Exception("Не переданы настройки");

        try {
            filesystem::fileWrite(SETTINGS_PATH . '/modules.json', json::encode($settings));
            self::$_settings = $settings;
        } catch (Exception $e) {
            throw new Exception(__('Не удалось сохранить настройки модулей (Проверьте права на запись в файл "%s")', filesystem::getRelPath(SETTINGS_PATH . '/modules.json')));
        }
    }

    /**
     * @throws Exception
     * @return \module[]
     */
    static function getAll()
    {
        $modules = array();
        $od = opendir(MODULES_PATH);
        while ($dir_name = readdir($od)) {
            if ($dir_name{0} === '.')
                continue;
            try {
                $module = new module(MODULES_PATH . '/' . $dir_name);
                //print_r($module->getRoutes());
                $modules[] = $module;
            } catch (Exception $e) {
                throw $e;
            }
        }
        closedir($od);
        return $modules;
    }

    /**
     * @param int $sort
     * @throws Exception
     * @return \module[]
     */
    static function getInstalled($sort = self::SORT_BY_CONTROLLER)
    {
        //return self::getAll();
        $modules = array();
        $settings = self::_getSettings();

        switch ($sort) {
            case self::SORT_BY_CONTROLLER:
                uasort($settings, array('self', '_cmp_controller'));
                break;
            case self::SORT_BY_REQUEST:
                uasort($settings, array('self', '_cmp_request'));
                break;
            case self::SORT_BY_RESPONSE:
                uasort($settings, array('self', '_cmp_response'));
                break;
            case self::SORT_BY_HOOK:
                uasort($settings, array('self', '_cmp_hook'));
                break;
        }

        foreach ($settings AS $module_name => $installed_config) {
            try {
                $modules[] = new module(MODULES_PATH . '/' . $module_name, $installed_config);
            } catch (Exception $e) {
                throw $e;
            }
        }

        return $modules;
    }

    /**
     * @throws Exception
     * @return \module
     */
    static function getModuleByRequest()
    {
        $modules_by_request = self::getModulesByRequest();
        if (!count($modules_by_request))
            throw new Exception(__("Отсутствует обработчик для запроса %s", request::getPath()));
        return $modules_by_request[0];
    }

    /**
     * @return \module[]
     */
    static function getModulesByRequest()
    {
        $modules_by_request = array();
        $modules = self::getInstalled();
        foreach ($modules AS $module) {
            if ($module->canExecuteRequest())
                $modules_by_request[] = $module;
        }
        return $modules_by_request;
    }

    /**
     * @return \module[]
     */
    static function getRequestPreprocessModules()
    {
        $modules = self::getInstalled(self::SORT_BY_REQUEST);
        $rpm = array();
        foreach ($modules AS $module) {
            if ($module->getRequestPreprocessMethodName())
                $rpm[] = $module;
        }
        return $rpm;
    }

    /**
     * @return \module[]
     */
    static function getResponsePostprocessModules()
    {
        $modules = self::getInstalled(self::SORT_BY_RESPONSE);
        $rpm = array();
        foreach ($modules AS $module) {
            if ($module->getResponsePostprocessMethodName())
                $rpm[] = $module;
        }
        return $rpm;
    }

    /**
     * @param $hook_name string
     * @return \module[]
     */
    static function getModulesByHookName($hook_name)
    {
        $modules = self::getInstalled(self::SORT_BY_HOOK);
        $modules_with_hook = array();
        foreach ($modules AS $module) {
            if (in_array($hook_name, $module->getHooks()))
                $modules_with_hook[] = $module;
        }
        return $modules_with_hook;
    }

    /**
     * Предварительная обработка запроса
     */
    static function executePreprocess()
    {
        $request_preprocess_modules = self::getRequestPreprocessModules();
        foreach ($request_preprocess_modules AS $module) {
            $module->executePreprocess();
        }
    }

    /**
     * непосредственная обработка запроса. Выполнение get, post методов модуля
     * @return \response
     */
    static function executeRequest()
    {
        $response = new response();
        $module = self::getModuleByRequest();
        $module->executeRequest($response);
        return $response;
    }

    /**
     * Постобработка ответа
     * @param response $response
     */
    static function executePostprocess(response $response)
    {
        $response_postprocess_modules = self::getResponsePostprocessModules();
        foreach ($response_postprocess_modules AS $module) {
            $module->executePostprocess($response);
        }
    }

    /**
     * Выполнение хуков, на которые подписаны другие модули
     * @param string $hook_name
     * @param mixed $hook_data
     * @throws Exception
     */
    static function executeHook($hook_name, &$hook_data = array())
    {
        if (!$hook_name)
            throw new Exception(__('Не указано имя хука'));

        if ($hook_name != '*') {
            // при выполнении любого хука выполняется хук "*"
            self::executeHook('*', $tmp_arr = array(
                'hook_name' => $hook_name,
                'hook_data' => $hook_data
            ));
        }

        $hook_modules = self::getModulesByHookName($hook_name);
        foreach ($hook_modules AS $module) {
            $module->executeHook($hook_name, $hook_data);
        }
    }

    /**
     * @param $module \module
     * @return bool
     */
    static function isInstalled(module $module)
    {
        return array_key_exists($module->getName(), self::_getSettings());
    }

    /**
     * Возвращает список структур таблиц, соответствующий установленным модулям
     * @param string|string[] $skip Список имен (или имя) пропускаемых модулей
     * @return \db_table[]
     */
    static protected function _getDbStructure($skip = array())
    {
        $skip = (array) $skip;
        $modules = self::getInstalled();
        $db_tables_common = array();
        foreach ($modules as $module) {
            if (in_array($module->getName(), $skip))
                continue;

            $db_tables = $module->getDbStructure();
            foreach ($db_tables as $db_table) {
                $table_name = $db_table->getName();
                if (!array_key_exists($table_name, $db_tables_common)) {
                    $db_tables_common[$table_name] = new db_table(array('name' => $table_name));
                }
                $db_tables_common[$table_name]->addStricture($db_table);
            }
        }
        return $db_tables_common;
    }

    /**
     * @param $module \module
     * @throws Exception
     */
    static function install(module $module)
    {
        if (self::isInstalled($module))
            throw new Exception(__('Модуль "%s" уже установлен', $module->getName()));

        if (cache_in_file::get('modules', 'installing', false))
            throw new Exception(__('В данный момент устанавливается другой модуль'));

        cache_in_file::set('modules', 'installing', true, 60);

        try {
            $settings = self::_getSettings();
            $settings[$module->getName()] = $module->getConfig();

            try {
                self::_setSettings($settings);
                $db_tables_current = self::_getDbStructure($module->getName());
                $db_tables_to = self::_getDbStructure();
                $rollback_sqls = array();

                try {
                    foreach ($db_tables_to as $db_table_to) {
                        if (array_key_exists($db_table_to->getName(), $db_tables_current)) {
                            $db_table_current = $db_tables_current[$db_table_to->getName()];
                            // запрос на модификацию таблицы
                            $sql = $db_table_current->getSqlQueryModifyTo($db_table_to);
                            $rollback_sql = $db_tables_to->getSqlQueryModifyTo($db_table_current);
                        } else {
                            // запрос создание новой таблицы
                            $sql = $db_table_to->getSqlQueryCreate();
                            $rollback_sql = $db_table_to->getSqlQueryDrop();
                        }
                        if (!db::me()->query($sql)) {
                            throw new Exception(__('Не удалось выполнить SQL запрос "%s"', $sql));
                        }
                        $rollback_sqls[] = $rollback_sql; // для каждого выполненного запроса сохраняем запрос для отката
                    }
                } catch (Exception $e) {
                    // откатываем все изменения в базе данных при ошибке в одном из запросов
                    foreach ($rollback_sqls as $sql) {
                        db::me()->quote($sql);
                    }
                    throw $e;
                }
            } catch (Exception $ex) {
                // откат установки модуля
                unset($settings[$module->getName()]);
                self::_setSettings($settings);
                throw $ex;
            }
        } catch (Exception $e) {
            // отменяем установку
            cache_in_file::set('modules', 'installing', false, 0);
            throw $e;
        }
        cache_in_file::set('modules', 'installing', false, 0);
    }

    /**
     * @param $module \module
     * @throws Exception
     */
    static function uninstall(module $module)
    {
        if (!self::isInstalled($module))
            throw new Exception(__('Модуль "%s" не установлен', $module->getName()));
    }

    /**
     * @param $module \module
     */
    static function remove(module $module)
    {
        self::uninstall($module);
    }

    /**
     * @param string $name
     * @throws Exception
     * @return \module
     */
    public static function getByName($name)
    {
        $modules = self::getAll();
        foreach ($modules AS $module) {
            if ($module->getName() === $name) {
                return $module;
            }
        }
        throw new Exception(__('Модуль "%s" не найден', $name));
    }

    /**
     * Рекурсивное получение зависимых модулей
     * @param module $module
     * @return module[]
     * @throws Exception
     */
    public static function getModuleDependencies(module $module)
    {
        $dependencies_all = array();
        $dependencies_module = $module->getDependencies();
        foreach ($dependencies_module as $module_name) {
            try {
                $module_d = self::getByName($module_name);
            } catch (Exception $ex) {
                throw new Exception(__('Не найден зависимый модуль "%s" для модуля "%s"', $module_name, $module->getName()));
            }

            $dependencies_all = array_merge($dependencies_all, modules::getModuleDependencies($module_d));
        }
        return array_unique($dependencies_all);
    }

    public static function getClasses()
    {
        static $classes = null;
        if (is_null($classes)) {
            $classes = array();
            $modules = self::getInstalled();
            foreach ($modules as $module) {
                $classes = array_merge($classes, $module->getClasses());
            }
        }
        return $classes;
    }
}