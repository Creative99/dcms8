<?php

class jquery_controller extends controller
{
    /**
     * @param $page_meta\page_meta
     * @return \page_meta
     */
    function theme_dependence_jquery($page_meta)
    {
        $page_meta->scripts[] = $this->getCurrentPathRel() . '/files/jquery-2.1.1.min.js';
    }

}