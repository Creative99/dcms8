<?php

/*
  Класс для подключения к БД
  Можно использовать в любом месте движка
  $db = DB::me();
 */

class DB
{
    static protected $_pdo_instance;

    /**
     * @return PDO
     * @throws Exception
     */
    static public function me()
    {
        if (is_null(self::$_pdo_instance)) {
            if (!class_exists('pdo'))
                throw new Exception(__("Отсутствует драйвер PDO"));

            $db_settings = system::getProperty('database');

            if (array_search($db_settings['driver'], PDO::getAvailableDrivers()) === false)
                throw new Exception(__("Отсутствует %s драйвер PDO", $db_settings['driver']));

            switch ($db_settings['driver']) {
                case 'mysql':
                    self::$_pdo_instance = new PDO('mysql:host=' . $db_settings['host'] . ';dbname=' . $db_settings['dbname'], $db_settings['user'], $db_settings['password']);
                    break;
                case 'sqlite':
                    self::$_pdo_instance = new PDO('sqlite:filename=' . SETTINGS_PATH . '/database.sqlite');
                    break;
            }

            self::$_pdo_instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            self::$_pdo_instance->query("SET NAMES utf8;");
        }


        return self::$_pdo_instance;
    }

    static public function isConnected()
    {
        return !is_null(self::$_pdo_instance);
    }

    protected function __construct()
    {
        
    }
}