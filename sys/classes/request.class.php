<?php

abstract class request
{
    static protected
            $get,
            $post,
            $path;

    static public function getMethod()
    {
        return strtolower(filter_input(INPUT_SERVER, 'REQUEST_METHOD'));
    }

    static public function getPath()
    {
        if (is_null(self::$path)) {
            self::$path = '/' . filter_input(INPUT_GET, 'DCMS_ROUTE');
        }
        return self::$path;
    }

    static public function getPathArray()
    {
        return explode('/', self::getPath());
    }

    static public function getGetVars()
    {
        if (is_null(self::$get)) {
            self::$get = filter_input_array(INPUT_GET);
        }
        return self::$get;
    }

    static public function getPostVars()
    {
        if (is_null(self::$post)) {
            self::$post = filter_input_array(INPUT_POST);
        }
        return self::$post;
    }

    static public function getModelKey()
    {
        return filter_input(INPUT_SERVER, 'HTTP_X_DCMS_MODEL_KEY');
    }
}